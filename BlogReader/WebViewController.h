//
//  WebViewController.h
//  BlogReader
//
//  Created by Noel Hwande on 6/5/13.
//  Copyright (c) 2013 Lutheran Hour Ministries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController

@property (strong, nonatomic) NSURL *blogPostURL;
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end
