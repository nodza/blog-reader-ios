//
//  TableViewController.h
//  BlogReader
//
//  Created by Noel Hwande on 5/17/13.
//  Copyright (c) 2013 Lutheran Hour Ministries. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewController : UITableViewController

@property (nonatomic, strong) NSMutableArray * blogPosts;

@end
